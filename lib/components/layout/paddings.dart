
import 'package:flutter/material.dart';

class Paddings extends StatelessWidget {

  final List<EdgeInsets> paddings;
  final Widget child;

  Paddings({this.paddings, this.child});

  static double getHeight(List<EdgeInsets> _paddings) => _paddings.fold<double>(0, (a, b) => a + b.vertical);
  static double getWidth(List<EdgeInsets> _paddings) => _paddings.fold<double>(0, (a, b) => a + b.horizontal);

  @override
  Widget build(BuildContext context) => paddings.fold<Widget>(child, (a, b) => Padding(padding: b, child: a));
}