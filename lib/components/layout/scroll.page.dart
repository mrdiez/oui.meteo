import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/components/layout/sliver.view.dart';

class ScrollPage extends StatelessWidget {

  final Widget child;
  final Widget appBar;
  final bool fillRemaining;
  final bool overlap;
  final List<Widget> slivers;
  final Function onRefresh;
  final GlobalKey<RefreshIndicatorState> refreshKey;

  ScrollPage({this.child, this.appBar, this.fillRemaining = false, this.overlap = false, this.slivers, this.onRefresh, this.refreshKey});

  Widget wrapWithRefresh(BuildContext context, { Widget child }) => onRefresh != null ? RefreshIndicator(
      key: refreshKey,
      displacement: MediaQuery.of(context).size.width / 2,
      child: child,
      onRefresh: onRefresh
  ) : child;

  @override
  Widget build(BuildContext context) {
    Widget _overlap = overlap ? SliverView.overlap(context) : null;

    return wrapWithRefresh(context,
      child: CustomScrollView(
        key: PageStorageKey<int>(hashCode), // A unique tab id to remember scroll position
        slivers: [
          if (overlap && _overlap != null) _overlap,
          if (appBar != null) appBar,
          if (slivers != null && slivers.length > 0) ...slivers,
          if (child != null && !fillRemaining) SliverToBoxAdapter(child: child),
          if (child != null && fillRemaining) SliverFillRemaining(child: child),
        ],
      ),
    );
  }
}
