import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/components/buttons/side.menu.button.dart';
import 'package:oui_sncf_weather/components/images/main.logo.dart';
import 'package:oui_sncf_weather/constants/config/routes.dart';
import 'package:oui_sncf_weather/constants/config/test.keys.dart';
import 'package:oui_sncf_weather/constants/theme/theme.sizes.dart';
import 'package:oui_sncf_weather/services/cache.dart';
import 'package:oui_sncf_weather/services/layout.dart';
import 'package:oui_sncf_weather/services/logout.dart';
import 'package:oui_sncf_weather/services/route.dart';
import 'package:oui_sncf_weather/services/translation.dart';

class SideMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    LayoutService(context).listen;
    Map<String, String> translate = TranslationService(context).get;
    RouteService routeService = RouteService(context).get;

    return ClipRRect(
      borderRadius: ThemeSizes.borderRadiusHorizontal[Sizes.Zero][Sizes.L],
      child: Drawer(
        child: Container(
          child: SafeArea(
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                Padding(
                  padding: ThemeSizes.padding[Sizes.S],
                  child: DrawerHeader(child: MainLogo(axis: Axis.vertical)),
                ),
                ...CacheService(context)
                    .get
                    .weatherCities
                    .map((city) => SideMenuButton(
                          title: city,
                          action: () => routeService.get.goTo(Routes.Weather[city]),
                        ))
                    .toList(),
                SideMenuButton(
                  testKey: TestKey(key: Keys.SideMenuButton, key2: Keys.SettingsButton),
                  title: translate['Settings'],
                  icon: Icons.settings,
                  action: () => routeService.get.goTo(Routes.Settings),
                ),
                SideMenuButton(
                  testKey: TestKey(key: Keys.SideMenuButton, key2: Keys.LogOutButton),
                  title: translate['Log out'],
                  icon: Icons.directions_run,
                  action: () => LogoutService.exit(context),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
