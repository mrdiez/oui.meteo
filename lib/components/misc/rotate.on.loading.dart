
import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/services/loading.dart';

class RotateOnLoading extends StatefulWidget {

  RotateOnLoading({
    this.loadingIds,
    this.force = false,
    this.child,
    this.testKey
  });

  final List<dynamic> loadingIds;
  final bool force;
  final Widget child;
  final String testKey;

  @override
  _RotateOnLoadingState createState() => _RotateOnLoadingState();
}

class _RotateOnLoadingState extends State<RotateOnLoading> with SingleTickerProviderStateMixin {

  LoadingService _loadingService;
  AnimationController rotationController;

  @override
  void initState() {
    super.initState();
    rotationController = AnimationController(duration: const Duration(seconds: 2), vsync: this);
  }

  void play() {
    rotationController.repeat();
  }

  void pause() {
    rotationController.reset();
  }

  bool _isLoading() {

    if (widget.force) return true;

    bool result = false;

    widget.loadingIds?.forEach((loadingId) {
      if(_loadingService.loading(loadingId)) {
        result = true;
      }
    });

    return result;

  }

  @override
  void dispose() {
    rotationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _loadingService = LoadingService(context).listen;

    _isLoading() ? play() : pause();

    return RotationTransition(
      key: Key(widget.testKey),
      alignment: Alignment.center,
      turns: Tween(begin: 0.0, end: 1.0).animate(rotationController),
      child: widget.child,
    );
  }
}