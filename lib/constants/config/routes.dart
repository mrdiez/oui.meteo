import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/features/login/login.dart';
import 'package:oui_sncf_weather/features/settings/settings.dart';
import 'package:oui_sncf_weather/features/weather/details/weather.details.dart';
import 'package:oui_sncf_weather/features/weather/list/weather.list.dart';
import 'package:oui_sncf_weather/models/route.data.dart';
import 'package:oui_sncf_weather/services/route.dart';

import '../../features/home/home.dart';

// ignore_for_file: non_constant_identifier_names
abstract class Routes {
  static Map<String, Widget Function(BuildContext)> Pages = {};
  static Map<String, List<Guard>> Guards = {};
  static Map<String, List<Guard>> PopGuards = {};
  static Map<String, List<RouteParams>> Params = {};
  static Map<String, List<String>> Histories = {};

  static Widget _commonWrapper(BuildContext context, Widget child) {
    return WillPopScope(
      onWillPop: () async {
        RouteService(context).get.pop();
        return false;
      },
      child: child,
    );
  }

  static Widget Function(BuildContext) _render(Widget Function() renderPage) {
    return (context) => _commonWrapper(context, renderPage());
  }

  static final String Login = RouteDataModel(
    name: '/',
    widget: _render(() => LoginPage()),
  ).name;

  static final String Home =
      RouteDataModel(name: '/home', widget: _render(() => HomePage()), guards: HomePage.guards, onPop: HomePage.onPop)
          .name;

  static final String Details = RouteDataModel(name: '/details', widget: _render(() => WeatherDetailsPage()), params: [
    RouteParams.ItemId,
  ]).name;

  static final String Settings = RouteDataModel(
    name: '/settings',
    widget: _render(() => SettingsPage()),
  ).name;

  static MapEntry<String, String> getWeatherRouteMap(String city) => MapEntry(
      city,
      RouteDataModel(
              name: '/${city.toLowerCase()}',
              widget: _render(() => WeatherListModule(city)),
              onPop: WeatherListModule.onPop)
          .name);

  static Map<String, String> initWeatherRoutes(List<String> cities) =>
      Weather = Map.fromEntries(cities.map(getWeatherRouteMap));

  static Map<String, String> Weather = initWeatherRoutes(["Paris", "New York", "Sidney", "Tokyo"]);
}

enum RouteParams { LoggedOut, ItemId, City }
