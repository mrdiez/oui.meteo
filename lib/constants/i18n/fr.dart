
const Map<String, String> FRENCH = {


  //--------------------- LOGIN ------------------------

  "Log in": "Se connecter",
  "Error, check your connection\nor try again later.": "Erreur, vérifiez votre connexion\nou réessayez plus tard.",

  "Remember me": "Se souvenir de moi",
  "Forgot your password ?": "Mot de passe oublié ?",

  "Reset my password": "Réinitialiser mon mot de passe",
  "Confirm your email :": "Confirmez votre email :",

  "Cancel": "Annuler",
  "Validate": "Valider",

  "A new password has been sent to you by email.": "Un nouveau mot de passe vous a été envoyé par email.",
  "The password associated with the email": "Le mot de passe associé à l'email",
  "could not be reset.": "n'a pas pu être réinitialisé.",



  //--------------------- HOME ------------------------

  "Welcome": "Bienvenue",
  "No city selected, please add some cities by going in Menu/Settings": "Aucune ville sélectionnée, merci d'en ajouter en allant dans Menu/Settings",



  //--------------------- WEATHER ------------------------

  "No weather found with this city": "Pas de météo trouvée pour cette ville",
  "thunderstorm with light rain": "orage avec pluie légère",
  "thunderstorm with rain": "orage avec pluie",
  "thunderstorm with heavy rain": "orage avec de fortes pluies",
  "light thunderstorm": "orage léger",
  "thunderstorm": "orage",
  "heavy thunderstorm": "orage violent",
  "ragged thunderstorm": "orage irrégulier",
  "thunderstorm with light drizzle": "orage avec légère bruine",
  "thunderstorm with drizzle": "orage avec bruine",
  "thunderstorm with heavy drizzle": "orage avec de fortes bruines",
  "light intensity drizzle": "bruine d'intensité lumineuse",
  "drizzle": "bruine",
  "heavy intensity drizzle": "bruine intense",
  "light intensity drizzle rain": "faible bruine et pluie",
  "drizzle rain": "bruine et pluie",
  "heavy intensity drizzle rain": "forte bruine et pluie",
  "shower rain and drizzle": "averses et bruine",
  "heavy shower rain and drizzle": "fortes averses et bruine",
  "shower drizzle": "averses et bruine",
  "light rain": "pluie légère",
  "rain": "pluie",
  "moderate rain": "pluie modérée",
  "heavy intensity rain": "pluie forte",
  "very heavy rain": "très forte pluie",
  "extreme rain": "pluie extrême",
  "freezing rain": "pluie verglaçante",
  "light intensity shower rain": "faibles averses",
  "shower rain": "averses",
  "heavy intensity shower rain": "fortes averses",
  "ragged shower rain": "averses éparses",
  "light snow": "neige légère",
  "snow": "neige",
  "Snow": "Neige",
  "Heavy snow": "Beaucoup de neige",
  "Sleet": "Grésil léger",
  "Light shower sleet": "Averses de grésil",
  "Shower sleet": "Légère pluie et neige",
  "Light rain and snow": "Pluie et neige",
  "Rain and snow": "Légères averses de neige",
  "Light shower snow": "Light shower snow",
  "Shower snow": "Averses de neige",
  "Heavy shower snow": "Fortes averses de neige",
  "mist": "brume",
  "Smoke": "Fumée",
  "Haze": "Brume",
  "sand/ dust whirls": "tourbillons de sable / poussière",
  "fog": "brouillard",
  "sand": "sable",
  "dust": "poussière",
  "volcanic ash": "cendre volcanique",
  "squalls": "grains",
  "tornado": "tornade",
  "clear sky": "ciel clair",
  "few clouds: 11-25%": "quelques nuages: 11-25%",
  "scattered clouds: 25-50%": "nuages épars: 25-50%",
  "broken clouds: 51-84%": "nuages fragmentés: 51-84%",
  "overcast clouds: 85-100%": "couvert: 85-100%",
  "few clouds": "quelques nuages",
  "scattered clouds": "nuages épars",
  "broken clouds": "nuages fragmentés",
  "overcast clouds": "couvert",



  //--------------------- SETTINGS ------------------------

  "Settings": "Réglages",
  "Add a city here": "Ajoutez une ville ici",
  "Theme": "Thème",
  "Light": "Clair",
  "Dark": "Sombre",
  "day1": "Lundi",
  "day2": "Mardi",
  "day3": "Mercredi",
  "day4": "Jeudi",
  "day5": "Vendredi",
  "day6": "Samedi",
  "day7": "Dimanche",



  //--------------------- COMPONENTS ------------------------


  // SIDE MENU (DRAWER)

  "Log out": "Se déconnecter",


  // EMAIL TEXT INPUT

  "E-mail": "E-mail",
  "Username": "Nom d'utilisateur",
  "Invalid e-mail": "E-mail invalide",


  // PASSWORD TEXT INPUT

  "Password": "Mot de passe",
  "characters minimum": "caractères minimum",
  "Incorrect credentials": "Identifiants incorrects",



  //--------------------- TOOLS ------------------------

  // DATE

  "Tomorrow": "Demain",
  "Today": "Aujourd'hui",
  "Yesterday": "Hier",
  "at": "à",



  //--------------------- SERVICES ------------------------

  "Thank you to grant location permission to our application in your device settings": "Merci de nous autoriser à accéder à votre position dans les réglages de votre téléphone",
  "Thank you to enable location service": "Merci d'activer le service de géolocalisation de votre téléphone"


};
