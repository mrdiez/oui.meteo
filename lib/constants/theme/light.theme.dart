// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/constants/theme/theme.colors.dart';
import 'package:oui_sncf_weather/constants/theme/theme.sizes.dart';

import 'main.theme.dart';

ThemeData get LightTheme => MainTheme.copyWith(
    appBarTheme: MainTheme.appBarTheme.copyWith(
      color: ThemeColors.Lightest,
    ),
    brightness: Brightness.light,
    primaryColor: ThemeColors.Primary,
    primaryColorLight: ThemeColors.Primary,
    primaryColorDark: ThemeColors.PrimaryVariant,
    accentColor: ThemeColors.Secondary,
    cardColor: ThemeColors.Lightest,
    canvasColor: ThemeColors.Light,
    colorScheme: MainTheme.colorScheme.copyWith(onPrimary: ThemeColors.Dark),
    inputDecorationTheme: MainTheme.inputDecorationTheme.copyWith(
      fillColor: ThemeColors.Lightest,
    ),
    iconTheme: MainTheme.iconTheme.copyWith(
      color: ThemeColors.Primary,
    ),
    chipTheme: MainTheme.chipTheme.copyWith(
        backgroundColor: ThemeColors.SecondaryVariant,
        labelStyle: (MainTheme.chipTheme.labelStyle ?? TextStyle()).copyWith(color: ThemeColors.Light)),
    tabBarTheme: MainTheme.tabBarTheme.copyWith(
      indicator: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: ThemeColors.Secondary,
            width: ThemeSizes.divider[Sizes.M],
          ),
        ),
      ),
      labelStyle: MainTheme.tabBarTheme.labelStyle.copyWith(color: ThemeColors.Dark),
    ),
    shadowColor: ThemeColors.LightestGrey,
    buttonTheme: MainTheme.buttonTheme.copyWith(
      buttonColor: ThemeColors.Primary,
    ),
    buttonColor: ThemeColors.Primary,
    indicatorColor: ThemeColors.SecondaryVariant,
    toggleableActiveColor: ThemeColors.Primary,
    textTheme: MainTheme.textTheme.copyWith(
      overline: MainTheme.textTheme.overline.copyWith(
        color: ThemeColors.DarkGrey,
      ),
      caption: MainTheme.textTheme.caption.copyWith(
        color: ThemeColors.Dark,
      ),
      button: MainTheme.textTheme.button.copyWith(
        color: ThemeColors.Light,
      ),
      bodyText1: MainTheme.textTheme.bodyText1.copyWith(
        color: ThemeColors.Dark,
      ),
      bodyText2: MainTheme.textTheme.bodyText2.copyWith(
        color: ThemeColors.Dark,
      ),
      subtitle1: MainTheme.textTheme.subtitle1.copyWith(
        color: ThemeColors.Dark,
      ),
      subtitle2: MainTheme.textTheme.subtitle2.copyWith(
        color: ThemeColors.Dark,
      ),
      headline1: MainTheme.textTheme.headline1.copyWith(
        color: ThemeColors.Primary,
      ),
      headline2: MainTheme.textTheme.headline2.copyWith(
        color: ThemeColors.Primary,
      ),
      headline3: MainTheme.textTheme.headline3.copyWith(
        color: ThemeColors.Primary,
      ),
      headline4: MainTheme.textTheme.headline4.copyWith(
        color: ThemeColors.Primary,
      ),
      headline5: MainTheme.textTheme.headline5.copyWith(
        color: ThemeColors.Primary,
      ),
      headline6: MainTheme.textTheme.headline6.copyWith(
        color: ThemeColors.Primary,
      ),
    ));
