// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';

abstract class ThemeColors {
  // This constructor prevents instantiation and extension.
  // ignore: unused_element
  ThemeColors._();

  // MAIN COLORS

  static Color Primary = _Primary;
  static Color PrimaryVariant = _PrimaryVariant;

  static Color Secondary = _Secondary;
  static Color SecondaryVariant = _SecondaryVariant;

  static const Color _Primary = const Color.fromRGBO(234, 83, 49, 1);
  static const Color _PrimaryVariant = const Color.fromRGBO(158, 20, 58, 1);

  static const Color _Secondary = const Color.fromRGBO(252, 205, 38, 1);
  static const Color _SecondaryVariant = const Color.fromRGBO(242, 138, 43, 1);

  // static const Color _Tertiary = const Color.fromRGBO(234, 77, 148, 1);
  // static const Color _TertiaryVariant = const Color.fromRGBO(231, 59, 37, 1);


  // CUSTOM THEMES COLORS

  static Map<String, Function> Switcher = {
    "Orange/Yellow": () => _switcher(_Primary, _PrimaryVariant, _Secondary, _SecondaryVariant),
  };

  static void _switcher(Color primary, Color primaryVariant, Color secondary, Color secondaryVariant) {
    Primary = primary;
    PrimaryVariant = primaryVariant;
    Secondary = secondary;
    SecondaryVariant = secondaryVariant;
  }

  // COMMON COLORS

  // Actions
  static const Color Positive = Color.fromRGBO(53, 223, 144, 1);
  static const Color Negative = Color.fromRGBO(211, 47, 47, 1);

  // 50 shades of grey
  static const Color Lightest = Color.fromRGBO(255, 255, 255, 1);
  static const Color Light = Color.fromRGBO(251, 246, 237, 1);
  static const Color LightestGrey = Color.fromRGBO(211, 206, 197, 1);
  static const Color LightGrey = Color.fromRGBO(171, 166, 157, 1);
  static const Color MediumGrey = Color.fromRGBO(131, 126, 117, 1);
  static const Color DarkGrey = Color.fromRGBO(91, 86, 77, 1);
  static const Color DarkestGrey = Color.fromRGBO(51, 46, 37, 1);
  static const Color Dark = Color.fromRGBO(21, 16, 7, 1);
  static const Color Darkest = Color.fromRGBO(0, 0, 0, 1);

  // Disabled
  static Color DisabledColor = LightGrey.withOpacity(0.95);
  static Color DisabledFontColor = DarkGrey.withOpacity(0.5);

  // Misc
  static Color TooltipColor = DarkGrey.withOpacity(0.75);
  static const Color AvatarColor = LightestGrey;
  static const Color AvatarColorDark = LightGrey;
  static Color get Divider => Primary;
  static Color get TitleUnderline => Primary;
  static Color get Toast => Primary;

}
