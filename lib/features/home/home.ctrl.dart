import 'package:mvcprovider/mvcprovider.dart';
import 'package:oui_sncf_weather/constants/config/routes.dart';
import 'package:oui_sncf_weather/services/route.dart';

import 'home.model.dart';

class HomeCtrl extends MVC_Controller<HomeModel> {
  void onTabTap(int i) {
    if (i != model.currentTabIndex)
      model.currentTabIndex = i;
    else {
      RouteService(context).get.goTo(Routes.Weather.values.toList()[i]);
    }
  }
}
