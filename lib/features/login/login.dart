import 'package:mvcprovider/mvcprovider.dart';

import 'login.ctrl.dart';
import 'login.model.dart';
import 'login.view.dart';

class LoginPage extends MVC_Module<LoginModel, LoginView, LoginCtrl> {
  final LoginModel model = LoginModel();
  final LoginView view = LoginView();
  final LoginCtrl ctrl = LoginCtrl();

  final List<SingleChildWidget> providers = [];
}
