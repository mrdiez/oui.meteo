import 'package:mvcprovider/mvcprovider.dart';
import 'package:oui_sncf_weather/constants/config/routes.dart';
import 'package:oui_sncf_weather/constants/config/test.keys.dart';
import 'package:oui_sncf_weather/features/weather/list/weather.list.ctrl.dart';
import 'package:oui_sncf_weather/models/weather.data.dart';
import 'package:oui_sncf_weather/services/cache.dart';
import 'package:oui_sncf_weather/services/route.dart';

class WeatherListModel extends MVC_Model<WeatherListCtrl> {
  final String city;
  bool showAppBar = false;
  Features loadingId = Features.Weather;

  List<WeatherDataModel> _weathers;
  List<WeatherDataModel> get weathers => _weathers;
  set weathers(List<WeatherDataModel> value) {
    List<WeatherDataModel> result = [];
    if ((value?.length ?? 0) > 0) {
      int hour = value.last.date.hour;
      value.forEach((weather) {
        if (result.length == 0 || weather.date.hour == hour) result.add(weather);
      });
    }
    _weathers = result;
  }

  CacheService get cacheService => CacheService(context).get;

  WeatherListModel(this.city);

  void initAppBar([bool shouldRefresh = false]) {
    showAppBar = RouteService.history.last != Routes.Home;
    if (shouldRefresh) notifyListeners();
  }

  @override
  void init() {
    initAppBar();
    if (!cacheService.isWeatherListExpired(city)) {
      weathers = cacheService.weathersByCity[city]?.values?.toList();
    }
  }
}
