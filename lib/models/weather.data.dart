
class WeatherDataModel {

  WeatherDataModel({
    this.id,
    this.date,
    this.temperature,
    this.iconUrl,
    this.weather,
    this.description,
    this.windDirection,
    this.windSpeed,
  });

  final String id;
  final DateTime date;
  final double temperature;
  final String iconUrl;
  final String weather;
  final String description;
  final int windDirection;
  final double windSpeed;

  String get iconHero => 'ICON-$id';
  String get titleHero => 'TITLE-$id';
  String get subtitleHero => 'SUBTITLE-$id';


  static WeatherDataModel fromJson(Map<String, dynamic> data, String city) {

    if (data == null) return null;

    return WeatherDataModel(
      id: "$city-${data["dt"]}",
      date: DateTime.parse(data["dt_txt"]),
      temperature: (data["main"]["temp"] is int) ? (data["main"]["temp"] as int).toDouble() : data["main"]["temp"],
      iconUrl: "https://openweathermap.org/img/wn/${data["weather"][0]["icon"]}@4x.png",
      weather: data["weather"][0]["main"],
      description: data["weather"][0]["description"],
      windDirection: data["wind"]["deg"],
      windSpeed: (data["wind"]["speed"] is int) ? (data["wind"]["speed"] as int).toDouble() : data["wind"]["speed"],
    );
  }
}