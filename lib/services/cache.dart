
import 'package:oui_sncf_weather/constants/config/routes.dart';
import 'package:oui_sncf_weather/constants/config/storage.keys.dart';
import 'package:oui_sncf_weather/models/weather.data.dart';
import 'package:oui_sncf_weather/services/local.storage.dart';
import 'package:oui_sncf_weather/tools/array.dart';
import 'package:oui_sncf_weather/models/user.data.dart';
import 'package:mvcprovider/mvcprovider.dart';
import 'package:oui_sncf_weather/tools/log.dart';

class CacheService extends MVC_Notifier<CacheService> {

  CacheService([context]) : super(context);

  // USERS
  int _loggedUserId;
  int get loggedUserId => _loggedUserId;
  set loggedUserId(int value) {
    _loggedUserId = value;
    notifyListeners();
  }
  Map<int, UserDataModel> users = {};
  List<UserDataModel> get userList => ArrayTool.mapToList<UserDataModel>(users);
  bool get isLogged => loggedUserId != null && users.containsKey(loggedUserId);
  UserDataModel get loggedUser => isLogged ? users[loggedUserId] : null;
  String get sessionToken => loggedUser?.sessionToken;

  void addUsers(List<UserDataModel> _users) {
    users.addEntries(_users.map((user) => MapEntry(user.id, user)));
  }

  // WEATHER
  Map<String, Map<String, WeatherDataModel>> weathersByCity = {};
  Map<String, DateTime> lastWeatherListSync = {};
  Duration _weatherListExpiration = Duration(minutes: 30);

  bool isWeatherList(String weatherCity) => ArrayTool.notEmpty(weathersByCity[weatherCity]);

  bool isWeatherListExpired(String weatherCity) {
    DateTime lastSync = lastWeatherListSync[weatherCity];
    bool result = lastSync == null || DateTime.now().difference(lastSync).compareTo(_weatherListExpiration) > 0;
    return result;
  }

  Map<String, WeatherDataModel> get weathers => weathersByCity.values.reduce((a, b) => {...a, ...b});

  void setWeathers(String weatherCity, List<WeatherDataModel> _weathers) {
    weathersByCity[weatherCity] = Map.fromIterable(_weathers, key: (weather) => weather.id, value: (weather) => weather);
    Log.print(weathersByCity[weatherCity], title: "setWeathers $weatherCity");
  }

  // WEATHER CITIES
  List<String> weatherCities;
  Future<List<String>> getWeatherCities() async {
    weatherCities = await LocalStorageService.get(StorageKeys.WeatherCities);
    weatherCities ??= Routes.Weather.keys.toList();
    return weatherCities;
  }

  Future<void> addWeatherCity(String city) async {
    weatherCities.add(city);
    Routes.Weather.addEntries([Routes.getWeatherRouteMap(city)]);
    await LocalStorageService.store(StorageKeys.WeatherCities, weatherCities);
    notifyListeners();
  }

  Future<void> removeWeatherCity(String city) async {
    weatherCities.remove(city);
    Routes.Weather.remove(city);
    await LocalStorageService.store(StorageKeys.WeatherCities, weatherCities);
    notifyListeners();
  }

  // CLEAR CACHE
  void empty() {
    _loggedUserId = null;
    users = {};
    weathersByCity = {};
    lastWeatherListSync = {};
  }

}