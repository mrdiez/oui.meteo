
import 'package:oui_sncf_weather/tools/digit.dart';
import 'package:oui_sncf_weather/tools/text.dart';

class ArrayTool {

  static bool notEmpty(dynamic list) => list != null && list.length > 0;

  static List<T> findByIdList<T>(List<T> list, List<dynamic> values) {

    List<T> result = [];

    list.forEach((T item) {
      dynamic obj = item;
      if (values.contains(obj.id)) result.add(item);
    });

    return result;

  }

  static List<T> findById<T>(List<T> list, dynamic value) {

    List<T> result = [];

    list.forEach((T item) {
      dynamic obj = item;
      if (value == obj.id) result.add(item);
    });

    return result;

  }

  static T findOneById<T>(List<T> list, dynamic value) {

    T result;

    list.forEach((T item) {
      dynamic obj = item;

      if (value == obj.id) {
        result = item;
      }
    });

    return result;

  }

  static List<T> mapToList<T>(Map<dynamic, T> map) => List.from(map.entries.map((item) => item.value));

  static List<T> getMapValues<T>(Map<dynamic, T> map, List<dynamic> keys) {
    List<T> result = [];
    map.forEach((key, value) { if (keys.contains(key)) result.add(value); });
    return result;
  }

  static List<T> getRandomEntries<T>(List<T> array, int nbOfEntries) {
    assert(nbOfEntries <= array.length);

    if (array.length < 1) {
      return [];
    }

    List<T> result = [];
    for (int i = 0; i < nbOfEntries; i++) {
      result.add(getRandomEntry(array, result));
    }
    return result;
  }

  static T getRandomEntry<T>(List<T> array, [List<T> blackList]) {
    blackList ??= [];
    array = [...array]..removeWhere((e) => blackList.contains(e));
    int arrayLength = array.length - 1 > 0 ? array.length - 1 : 0;
    return array[DigitTool.getRandomNumber(arrayLength)];
  }

  /* arr[] ---> Input Array
    data[] ---> Temporary array to store current combination
    start & end ---> Staring and Ending indexes in arr[]
    index ---> Current index in data[]
    r ---> Size of a combination to be printed */
    static String _allCombinations(List<int> arr, List<int> data, int start,
    int end, int index, int r) {
      String res = "";
      // Current combination is ready to be printed, print it
      if (index == r) {
        for (int j=0; j<r; j++) {
          res += "${data[j]}";
          if (j < r - 1) res += ',';
        }

        res += " ";
        return res;
      }
    // replace index with all possible elements. The condition
    // "end-i+1 >= r-index" makes sure that including one element
    // at index will make a combination with remaining elements
    // at remaining positions
      for (int i=start; i<=end && end-i+1 >= r-index; i++) {
        data[index] = arr[i];
        res += _allCombinations(arr, data, i+1, end, index+1, r);
       }

      return res;
    }


    // The main function that prints all combinations of size r
    // in arr[] of size n. This function mainly uses combinationUtil()
    static List<List<int>> getAllCombinations(List<int> arr, int r) {
      // A temporary array to store all combination one by one
      List<int> data = List.filled(r, null);
      int n = arr.length;
      List<String> splittedRes = _allCombinations(arr, data, 0, n-1, 0, r).split(' ');

      splittedRes.removeLast();
      return splittedRes.map((str) {
        if (TextTool.notNull(str))
          return str.split(',').map((_str) {
            return int.parse(_str);
          } ).toList();
      }).toList();
    }
}