
import 'dart:math';
import 'package:oui_sncf_weather/tools/text.dart';

class DigitTool {

  static String getTwoDigitsDecimal(double n) => TextTool.removeLastZeros(n.toStringAsFixed(2));

  static int getRandomNumber(int max, [int min = 0]) => max > 0 ? Random().nextInt(max + 1) + min : 0;
}