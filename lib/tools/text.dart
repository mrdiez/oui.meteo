
class TextTool {

  static String capitalize(String str) => "${str[0].toUpperCase()}${str.substring(1)}";

  static String getPlural(int nb) => nb > 1 ? 's' : '';

  static String removeLast(String str) => str.substring(0, str.length - 1);

  static String removeLastZeros(String str) {
    if ((str.endsWith("0") && str.contains(".")) || str.endsWith(".") || str.endsWith(",") || str.endsWith(" ")) {
      return removeLastZeros(removeLast(str));
    }
    else
      return str;
  }

  static String removeAllAfter(String str, String target, [bool include = false]) {
    return str.substring(0, str.indexOf(target) - (include ? 1 : 0));
  }

  static bool notNull(String str) => str != null && str.length > 0 && str != 'null';

  static String getStringBetweenTwo(String str, String str1, String str2) {
    final startIndex = str.indexOf(str1);
    final endIndex = str.indexOf(str2, startIndex + str1.length);
    return (startIndex < 0 || endIndex < 0) ? null : str.substring(startIndex + str1.length, endIndex);
  }

  static String replaceStringBetweenTwo(String str, String replace, String str1, String str2) {
    final startIndex = str.indexOf(str1);
    final endIndex = str.indexOf(str2, startIndex + str1.length);

    if (startIndex < 0 || endIndex < 0) return str;

    final firstPart = str.substring(0, startIndex + str1.length);
    final endPart = str.substring(endIndex, str.length);

    return firstPart + replace + endPart;
  }

  static String joinNonNullStrings(List<String> stringList) => stringList.where((str) => TextTool.notNull(str)).join((", "));

  static String limitNbOfCharacters(String str, int limit) => str.substring(0, str.length >= limit ? limit : str.length) + (str.length > limit ? "..." : "");
}