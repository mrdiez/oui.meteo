
import 'package:flutter_driver/flutter_driver.dart';
import 'package:oui_sncf_weather/constants/config/test.keys.dart';
import 'package:oui_sncf_weather/constants/mocks/users.dart';
import 'package:test/test.dart';

import 'test.model.dart';

// Run test with : flutter drive --target=test_driver/main.dart --driver=test_driver/login_test.dart --dart-define=TESTS=true

void main() {
  group('Login Test', () {
    FlutterDriver driver;
    int testNo = 0;

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('#${testNo++} : email empty | password empty', () async {
      await LoginTest(TestDataModel(driver)).completeLoginTest(
        emailError: "Invalid e-mail",
        passwordError: "4 characters minimum"
      );
    });

    test('#${testNo++} : email valid | password empty', () async {
      await LoginTest(TestDataModel(driver)).completeLoginTest(
          email: "1234@1234.com",
          passwordError: "4 characters minimum"
      );
    });

    test('#${testNo++} : email empty | password valid', () async {
      await LoginTest(TestDataModel(driver)).completeLoginTest(
          emailError: "Invalid e-mail",
          password: "1234",
      );
    });

    test('#${testNo++} : email invalid | password invalid', () async {
      await LoginTest(TestDataModel(driver)).completeLoginTest(
          email: "123",
          emailError: "Invalid e-mail",
          password: "123",
          passwordError: "4 characters minimum"
      );
    });

    test('#${testNo++} : email valid | password invalid', () async {
      await LoginTest(TestDataModel(driver)).completeLoginTest(
          email: "1234@1234.com",
          password: "123",
          passwordError: "4 characters minimum"
      );
    });

    test('#${testNo++} : email invalid | password valid', () async {
      await LoginTest(TestDataModel(driver)).completeLoginTest(
        email: "123",
        emailError: "Invalid e-mail",
        password: "1234",
      );
    });

    test('#${testNo++} : email empty | password invalid', () async {
      await LoginTest(TestDataModel(driver)).completeLoginTest(
          emailError: "Invalid e-mail",
          password: "123",
          passwordError: "4 characters minimum"
      );
    });

    test('#${testNo++} : email invalid | password empty', () async {
      await LoginTest(TestDataModel(driver)).completeLoginTest(
          email: "123",
          emailError: "Invalid e-mail",
          passwordError: "4 characters minimum"
      );
    });

    test('#${testNo++} : email valid | password valid', () async {
      await LoginTest(TestDataModel(driver)).completeLoginTest(
          email: "1234@1234.com",
          emailError: "", // Ensure no message has been displayed but shake animation still triggered
          password: "1234",
          passwordError: "Incorrect credentials",
          shouldLoad: true,
      );
    });

    test('#${testNo++} : existing user', () async {
      await LoginTest(TestDataModel(driver,MockedUsers[0])).start(true);
    });

    test('#${testNo++} : existing user', () async {
      await LoginTest(TestDataModel(driver,MockedUsers[1])).start();
    });
  }, timeout: Timeout(Duration(seconds: 60)));
}

class LoginTest {
  final emailTextInputFinder = find.byValueKey(TestKey(feature: Features.Login, key: Keys.EmailTextInput));
  final emailErrorLabelFinder = find.byValueKey(TestKey(feature: Features.Login, key: Keys.EmailTextInput, key2: Keys.ErrorLabel));
  final passwordTextInputFinder = find.byValueKey(TestKey(feature: Features.Login, key: Keys.PasswordTextInput));
  final passordErrorLabelFinder = find.byValueKey(TestKey(feature: Features.Login, key: Keys.PasswordTextInput, key2: Keys.ErrorLabel));
  final backgroundLoaderFinder = find.byValueKey(TestKey(key: Keys.BackgroundLoader));

  FlutterDriver get driver => model.driver;
  final TestDataModel model;

  LoginTest(this.model);

  static void run() => main();

  Future<void> completeLoginTest({
    String email,
    String emailError,
    String password,
    String passwordError,
    bool shouldLoad = false,
    bool shouldSuccess = false,
    bool rememberMe = false,
  }) async {
    print("${model.step++} - Reset the view (to remove previous test error messages or animations)");
    await driver.requestData('restart');
    try {
      await driver.waitFor(find.text('Remember me'), timeout: Duration(seconds: 2));
    } catch(e) {
      await model.restart(false);
    }

    print("${model.step++} - Ensure that error messages aren't displayed");
    await driver.waitForAbsent(emailErrorLabelFinder);
    await driver.waitForAbsent(passordErrorLabelFinder);

    if (email != null) {
      print("${model.step++} - Input email & check if length is valid");
      await driver.tap(emailTextInputFinder);
      await driver.enterText(email);
      await driver.waitFor(find.text(email));
      if (email.length < 4)
        expect(await driver.getText(emailErrorLabelFinder), emailError);
    }
    else {
      print("${model.step++} - Empty email field");
      await driver.tap(emailTextInputFinder);
      await driver.enterText("");
    }

    if (password != null) {
      print("${model.step++} - Input password & check if length is valid");
      await driver.tap(passwordTextInputFinder);
      await driver.enterText(password);
      await driver.waitFor(find.text(password));
      if (password.length < 4)
        expect(await driver.getText(passordErrorLabelFinder), passwordError);
    }
    else {
      print("${model.step++} - Empty password field");
      await driver.tap(passwordTextInputFinder);
      await driver.enterText("");
    }

    if (rememberMe) {
      print("${model.step++} - Check the remember me button");
      await driver.tap(find.byValueKey(TestKey(feature: Features.Login, key: Keys.RememberMeButton)));
    }

    print("${model.step++} - Retrieve inputs position (to test shake animation)");
    DriverOffset emailOffset = await driver.getTopLeft(emailTextInputFinder);
    DriverOffset passwordOffset = await driver.getTopLeft(passwordTextInputFinder);

    print("${model.step++} - Submit the form");
    await driver.waitForAbsent(find.byValueKey(TestKey(key: Keys.BackgroundLoader)));
    await driver.tap(find.byValueKey(TestKey(feature: Features.Login, key: Keys.OkButton)));

    print("${model.step++} - Ensure email error message and animation are displayed if needed");
    if (emailError != null) {
      expect(emailOffset, isNot(equals(await driver.getTopLeft(emailTextInputFinder))));
      if (emailError.length > 0)
        expect(await driver.getText(emailErrorLabelFinder), emailError);
    }
    else if (!shouldSuccess)
      expect(emailOffset, equals(await driver.getTopLeft(emailTextInputFinder)));

    print("${model.step++} - Ensure password error message and animation are displayed if needed");
    if (passwordError != null) {
      expect(passwordOffset, isNot(equals(await driver.getTopLeft(passwordTextInputFinder))));
      if (passwordError.length > 0)
        expect(await driver.getText(passordErrorLabelFinder), passwordError);
    }
    else if (!shouldSuccess)
      expect(passwordOffset, equals(await driver.getTopLeft(passwordTextInputFinder)));

    if (!shouldSuccess) {
      if (shouldLoad) {
        print("${model.step++} - Ensure toast has been displayed with server message");
        await driver.waitFor(find.text('Invalid credentials'));
        print("${model.step++} - Ensure remember me button has been replaced with forgot password button");
        await driver.waitForAbsent(find.text('Remember me'));
        await driver.waitFor(find.text('Forgot your password ?'));
      }
      else {
        print("${model.step++} - Ensure remember me button is still here");
        await driver.waitFor(find.text('Remember me'));
      }
    }
  }

  Future<void> _checkStoredCredentials(bool rememberMe) async {
    if (rememberMe) {
      print("${model.step++} - Ensure credentials have been saved while remember me have been checked");
      await driver.waitFor(find.text(model.user.email));
      await driver.waitFor(find.text(model.user.password));

      if (rememberMe) {
        print("${model.step++} - Uncheck the remember me button");
        await driver.tap(find.byValueKey(TestKey(feature: Features.Login, key: Keys.RememberMeButton)));
      }
    } else {
      print("${model.step++} - Ensure credentials have not been saved");
      await driver.waitForAbsent(find.text(model.user.email));
      await driver.waitForAbsent(find.text(model.user.password));
    }
  }

  Future<void> start([bool rememberMe = false]) async {
    await completeLoginTest(
        email: model.user.email,
        password: model.user.password,
        shouldSuccess: true,
        rememberMe: rememberMe
    );

    await model.checkPage('HomePage');

    await model.logout();
    await _checkStoredCredentials(rememberMe);
  }
}