
import 'package:flutter_driver/driver_extension.dart';
import 'package:oui_sncf_weather/main.dart' as app;
import 'package:oui_sncf_weather/services/local.storage.dart';

// Run all tests with : flutter drive --target=test_driver/main.dart --driver=test_driver/all.dart --dart-define=TESTING=true

void main() {
  // This line enables the extension.
  enableFlutterDriverExtension(handler: (command) async {
    if (command == 'restart') {
      // To avoid reusing last saved credentials
      await LocalStorageService.clear();
      app.main();
      return command;
    }
    else throw Exception('Unknown command');
  });

  // Call the `main()` function of the app, or call `runApp` with
  // any widget you are interested in testing.
  app.main();
}